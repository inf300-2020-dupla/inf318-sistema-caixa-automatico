package sistemaCaixaAutomatico;

/**
 * Classe que implementa a CobrancaTaxaStrategy para definir a estratégia básica.
 * Foi utilizado valores aleatórios.
 * 
 * @author guilherme e arua
 *
 */
public class BasicStrategy implements CobrancaTaxaStrategy {
	private float mensalidade;
	private float valOperacao;
	private int qtdOperacoes;
	
	public BasicStrategy() {
		this.mensalidade = 20.0f;
		this.valOperacao = 2.0f;
		this.qtdOperacoes = 5;
	}
	
	@Override
	public float calcular() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	/**
	 * Método para verificar se o limite de operações acabou.
	 * 
	 * @return 
	 */
	private boolean cobrarOperacao() {
		return (this.qtdOperacoes <= 0);
	}
}
