package sistemaCaixaAutomatico;

/**
 * Interface que implementa a estratégia de cobrança de taxas
 * @author guilherme e aruã
 *
 */
public interface CobrancaTaxaStrategy {
	public float calcular();
}
