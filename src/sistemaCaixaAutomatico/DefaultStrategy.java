package sistemaCaixaAutomatico;

/**
 * Classe que implementa a CobrancaTaxaStrategy para definir a estratégia padrão.
 * 
 * @author guilherme e arua
 *
 */
public class DefaultStrategy implements CobrancaTaxaStrategy {
	private float valOperacao; // valor da taxa de operação
	
	public DefaultStrategy() {
		this.valOperacao = 5.0f;
	}

	@Override
	public float calcular() {
		return this.valOperacao;
	}

}
