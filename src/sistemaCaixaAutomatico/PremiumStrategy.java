package sistemaCaixaAutomatico;

/**
 * Classe que implementa a CobrancaTaxaStrategy para definir a estratégia premium.
 * Foi utilizado valores aleatórios.
 * 
 * @author guilherme e arua
 *
 */
public class PremiumStrategy implements CobrancaTaxaStrategy {
	private float mensalidade;
	
	public PremiumStrategy() {
		this.mensalidade = 30.0f;
	}

	@Override
	public float calcular() {
		// TODO Auto-generated method stub
		return 0;
	}

}
